#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "model:ir.message,text:msg_carrier_load_order_sale_state"
msgid "Cannot cancel Sale \"%(sale)s\" due to it is confirmed."
msgstr "No puede cancelar la Venta \"%(sale)s\" porque está confirmada."

msgctxt "model:res.group,name:group_force_undo_load"
msgid "Force undo load"
msgstr "Forzar reapertura de carga"

msgctxt "view:carrier.load.order:"
msgid "Shipment and Sale (if proceed) will be cancelled."
msgstr "Albarán y Venta (si procede) van a cancelarse."

msgctxt "view:carrier.load.order:"
msgid "Back to run"
msgstr "Volver a iniciar"

msgctxt "model:ir.model.button,string:carrier_load_order_run_button
msgid "Run"
msgstr "Ejecutar"
