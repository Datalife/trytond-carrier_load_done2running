# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import load


def register():
    Pool.register(
        load.LoadOrder,
        load.Sale,
        module='carrier_load_done2running', type_='model')
    Pool.register(
        load.LoadOrderProcessing2Confirmed,
        module='carrier_load_done2running', type_='model',
        depends=['sale_processing2confirmed'])
