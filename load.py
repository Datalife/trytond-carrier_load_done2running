# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'
    _sale_values_to_copy = {'number'}

    @classmethod
    def __setup__(cls):
        super(LoadOrder, cls).__setup__()
        cls._transitions.add(('done', 'running'))
        cls._buttons.update({
            'run': {
                'icon': 'tryton-back',
                'invisible': Eval('state') != 'done',
                'depends': ['state']
            },
        })

    def _get_load_sale(self, Sale):
        values = {}
        if self.sale and self.sale.state == 'cancelled' and \
                self.sale.origin.__name__ == 'carrier.load.order':
            for value in self._sale_values_to_copy:
                values[value] = getattr(self.sale, value, None)
            Sale.delete([self.sale])
        sale = super()._get_load_sale(Sale)
        for name, value in values.items():
            setattr(sale, name, value)
        return sale

    def _get_shipment_out(self, sale):
        Shipment = Pool().get('stock.shipment.out')

        number = None
        if self.shipment and self.shipment.state == 'cancelled':
            number = self.shipment.number
            Shipment.delete([self.shipment])
        shipment = super()._get_shipment_out(sale)
        if number:
            shipment.number = number
        return shipment

    def _get_shipment_internal(self):
        Shipment = Pool().get('stock.shipment.internal')

        number = None
        if self.shipment and self.shipment.state == 'cancelled':
            number = self.shipment.number
            Shipment.delete([self.shipment])
        shipment = super()._get_shipment_internal()
        if number:
            shipment.number = number
        return shipment

    def _update_sale(self, uls):
        if self.shipment and self.shipment.state == 'cancelled':
            return
        super()._update_sale(uls)

    @classmethod
    def run(cls, records):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Move = pool.get('stock.move')
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentInternal = pool.get('stock.shipment.internal')
        UnitLoad = pool.get('stock.unit_load')

        to_run = [r for r in records if r.state == 'done']
        if to_run:
            for item in to_run:
                if item.shipment:
                    item.shipment._check_cancel_ul_moves()
                if (item.sale and item.sale.origin == item
                        and item.sale.state not in ('draft', 'quotation')):
                    raise UserError(gettext(
                        'carrier_load_done2running.'
                        'msg_carrier_load_order_sale_state',
                        sale=item.sale.rec_name))

            sales = [r.sale for r in to_run if r.sale and r.sale.origin and
                r.sale.origin.__name__ == 'carrier.load.order']
            sale_lines = [line for sale in sales for line in sale.lines]
            shipments_out = [r.shipment for r in to_run
                if r.type == 'out' and r.shipment]
            shipments_internal = [r.shipment for r in to_run
                if r.type == 'internal' and r.shipment]
            moves = [m for s in shipments_out + shipments_internal
                for m in s.moves]
            order_moves = [m for r in to_run for m in r.outgoing_moves
                if not m.shipment]
            order_moves.extend([m for r in to_run for m in r.inventory_moves
                if not m.shipment])
            uls = list(set(m.unit_load for m in moves + order_moves
                if m.unit_load))
            with Transaction().set_context(
                    _check_access=False,
                    check_origin=False,
                    check_shipment=False):
                Move.cancel(moves + order_moves)
                Move.delete(moves + order_moves)
                if shipments_out:
                    ShipmentOut.cancel(shipments_out)
                if shipments_internal:
                    ShipmentInternal.cancel(shipments_internal)
            with Transaction().set_context(_check_access=False):
                if sales:
                    Sale.cancel(sales)
                if sale_lines:
                    SaleLine.delete(sale_lines)
            if uls:
                UnitLoad.set_at_warehouse(uls)

        super(LoadOrder, cls).run(records)

    @classmethod
    def delete(cls, records):
        pool = Pool()
        Sale = pool.get('sale.sale')
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentInternal = pool.get('stock.shipment.internal')

        sales = []
        shipments_out = []
        shipments_internal = []
        orders = []
        for record in records:
            if record.sale and record.sale.state == 'cancelled':
                sales.append(record.sale)
            if (record.type == 'out' and record.shipment
                    and record.shipment.state == 'cancelled'):
                shipments_out.append(record.shipment)
                orders.append(record)
            if (record.type == 'internal' and record.shipment
                    and record.shipment.state == 'cancelled'):
                shipments_internal.append(record.shipment)
                orders.append(record)

        with Transaction().set_context(_check_access=False):
            if sales:
                Sale.delete(sales)
            if shipments_out:
                ShipmentOut.delete(shipments_out)
            if shipments_internal:
                ShipmentInternal.delete(shipments_internal)
            if orders:
                cls.write(orders, {'shipment': None})

        super().delete(records)


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    quoted = fields.Boolean('Quoted', readonly=True)
    # allows to send a notification the first time quoted.
    # because we cannot base condition on number filled as it can
    # being set from load.

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        table_h = cls.__table_handler__(module_name)
        table = cls.__table__()

        column_exists = table_h.column_exist('quoted')

        super().__register__(module_name)

        if not column_exists:
            cursor.execute(*table.update(
                columns=[table.quoted],
                values=[True],
                where=table.state.in_([
                    'quotation', 'confirmed', 'processing', 'done']))
            )

    @classmethod
    def quote(cls, records):
        super().quote(records)
        for record in records:
            if not record.quoted:
                record.quoted = True
                record.save()

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('quoted', False)
        return super().copy(records, default=default)


class LoadOrderProcessing2Confirmed(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._sale_values_to_copy.add('invoice_cache')
